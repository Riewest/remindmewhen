﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Android.App;
using Android.Content;
using Android.Widget;
using Android.OS;
using Android.Runtime;
using RemindMeWhenApp.DataObjects;
using RemindMeWhenApp.Droid.Adapters;
using RemindMeWhenApp.Droid.Dialogs;
using RemindMeWhenApp.Droid.Services;
using RemindMeWhenApp.ServiceLayer;
using Result = Android.App.Result;

namespace RemindMeWhenApp.Droid
{
    [Activity(Label = "RemindMeWhen", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity
    {
        private const string TAG = "RMWMainActivity";
        private EditText datetimeEditText;
        private EditText reminderNoteEditText;
        private EditText locationEditText;
        private Button createReminderButton;
        private Button viewRemindersButton;
        private ListView upcomingRemindersListView;
        private RemindersListAdapter upcomingRemindersListAdapter;
        private DateTime selectedTime;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            RieLog.Info(TAG, "RemindMeWhenApp started");
            base.OnCreate(savedInstanceState);
            MainController.Init();
            SetContentView(Resource.Layout.Main);
            ReminderAlarmReciever.StartReminderService(this);
            initControls();
            updateScreen(true);
        }

        protected override void OnResume()
        {
            base.OnResume();
            updateScreen(false);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            if (resultCode == Result.Ok)
            {
                locationEditText.Text = data.GetStringExtra("LocationName");
            }
        }

        private void initControls()
        {

            datetimeEditText = FindViewById<EditText>(Resource.Id.timeETxt);
            reminderNoteEditText = FindViewById<EditText>(Resource.Id.reminderNoteETxt);
            locationEditText = FindViewById<EditText>(Resource.Id.locationETxt);
            createReminderButton = FindViewById<Button>(Resource.Id.createReminderBtn);
            viewRemindersButton = FindViewById<Button>(Resource.Id.viewRemindersBtn);
            upcomingRemindersListView = FindViewById<ListView>(Resource.Id.upcomingList);
            upcomingRemindersListAdapter = new RemindersListAdapter(this, MainController.GetUpcomingReminders().ToArray());
            upcomingRemindersListView.Adapter = upcomingRemindersListAdapter;
            datetimeEditText.Click += datetimeEditTextClick;
            datetimeEditText.FocusChange += (sender, args) => {if(args.HasFocus) datetimeEditTextClick(sender, args); };
            locationEditText.Click += locationEditTextClick;
            locationEditText.FocusChange += (sender, args) => {if (args.HasFocus) locationEditTextClick(sender, args); };
            createReminderButton.Click += createReminderButtonClick;
            viewRemindersButton.Click += viewReminderButtonClick;
            
        }

        private void createReminderButtonClick(object sender, EventArgs eventArgs)
        {
            string note = reminderNoteEditText.Text;
            ReminderLocation reminderLocation = MainController.Locations.Find(l => l.Name.Equals(locationEditText.Text));
            DateTime time = Convert.ToDateTime(datetimeEditText.Text);
            if (note.Replace(" ", "").Length <= 0)
            {
                Toast.MakeText(this, "Please Enter a Note.", ToastLength.Short).Show();
                return;
            }
            MainController.CreateReminder(new Reminder()
            {
                Note = note,
                ReminderLocation = reminderLocation,
                Time = time
            });
            updateScreen(true);
        }

        private void viewReminderButtonClick(object sender, EventArgs eventArgs)
        {
            Intent viewRemindersIntent = new Intent(this, typeof(ViewRemindersActivity));
            StartActivity(viewRemindersIntent);
        }

        private void locationEditTextClick(object sender, EventArgs eventArgs)
        {
            LocationDialog locationDialog = new LocationDialog(this, MainController.Locations);
            locationDialog.Dismissed += locationPicked;
            locationDialog.Show();
        }

        private void datetimeEditTextClick(object sender, EventArgs eventArgs)
        {

            DateTime today = DateTime.Now;
            selectedTime = DateTime.Today;
            TimePickerDialog timePicker = new TimePickerDialog(this, OnTimeSet, today.Hour, today.Minute, false);
            timePicker.Show();
        }

        private void OnDateSet(object sender, DatePickerDialog.DateSetEventArgs eventArgs)
        {
            var sDate = eventArgs.Date;
            selectedTime = new DateTime(sDate.Year, sDate.Month, sDate.Day, selectedTime.Hour, selectedTime.Minute, selectedTime.Second);
            if (selectedTime < DateTime.Now)
                selectedTime = selectedTime.AddDays(1);
            datetimeEditText.Text = selectedTime.ToString("h:mm tt MMM d");
        }

        private void OnTimeSet(object sender, TimePickerDialog.TimeSetEventArgs eventArgs)
        {
            
            selectedTime = selectedTime.AddHours(eventArgs.HourOfDay);
            selectedTime = selectedTime.AddMinutes(eventArgs.Minute);
            Java.Util.Calendar calendar = Java.Util.Calendar.GetInstance(Java.Util.TimeZone.Default);
            long minDate = calendar.TimeInMillis;
            DateTime today = DateTime.Now;
            DatePickerDialog datePicker = new DatePickerDialog(this, OnDateSet, today.Year, today.Month - 1, today.Day);
            datePicker.DatePicker.MinDate = minDate;
            datePicker.Show();
        }

        private void locationPicked(object sender, DialogEventArgs eventArgs)
        {
            if (eventArgs.ReminderLocation != null)
            {
                locationEditText.Text = eventArgs.ReminderLocation.Name;
            }
            else
            {
                Intent newLocationIntent = new Intent(this, typeof(NewLocationActivity));
                StartActivityForResult(newLocationIntent, 0);
            }
        }

        private void updateScreen(bool clearFields)
        {
            if (clearFields)
            {
                reminderNoteEditText.Text = "";
                datetimeEditText.Text = DateTime.Now.AddHours(1).ToString("h:mm tt MMM d");
                locationEditText.Text = "";
            }

            var upcomingReminders = MainController.GetUpcomingReminders();
            upcomingRemindersListAdapter.ResetItems(upcomingReminders.ToArray());
        }

        
        
    }
}

