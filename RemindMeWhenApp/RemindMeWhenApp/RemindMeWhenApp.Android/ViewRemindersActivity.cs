﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RemindMeWhenApp.DataObjects;
using RemindMeWhenApp.Droid.Adapters;
using RemindMeWhenApp.ServiceLayer;

namespace RemindMeWhenApp.Droid
{
    [Activity(Label = "ViewRemindersActivity")]
    public class ViewRemindersActivity : Activity
    {
        private List<Reminder> toDelete;
        private ListView remindersListView;
        private RemindersListAdapter adapter;
        private IMenu optionsMenu;
        private bool deleteMode = false;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.ViewReminders);
            toDelete = new List<Reminder>();
            initControls();
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.top_menus, menu);
            optionsMenu = menu;
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.action_delete:
                    deleteItems();
                    break;
            }

            return base.OnOptionsItemSelected(item);
        }
        

        private void initControls()
        {
            remindersListView = FindViewById<ListView>(Resource.Id.remindersListView);
            adapter = new RemindersListAdapter(this, MainController.Reminders.ToArray());
            remindersListView.Adapter = adapter;
            remindersListView.ItemClick += remindersListViewItemClick;
        }

        private void remindersListViewItemClick(object sender, AdapterView.ItemClickEventArgs eventArgs)
        {
            if (deleteMode)
            {
                var reminder = adapter[eventArgs.Position];
                toDelete.Add(reminder);
                adapter.ItemClick(sender, eventArgs);
            }
        }

        private void deleteItems()
        {
            if (deleteMode)
            {
                optionsMenu.FindItem(Resource.Id.action_delete).SetIcon(Resource.Mipmap.ic_action_delete);
                int count = toDelete.Count;
                if (count > 0)
                {
                    AlertDialog.Builder deleteReminderAlert = new AlertDialog.Builder(this);
                    deleteReminderAlert.SetTitle("Confirm Delete");
                    deleteReminderAlert.SetMessage($"Are you sure you want to {count} reminder{(count == 1 ? "" : "s")}");
                    deleteReminderAlert.SetPositiveButton("Delete",
                        ((sender, args) =>
                        {
                            MainController.DeleteReminders(toDelete);
                            toDelete = new List<Reminder>();
                            Toast.MakeText(this, $"{count} Reminder{(count == 1 ? "" : "s")} Deleted", ToastLength.Long)
                                .Show();
                        }));
                    deleteReminderAlert.SetNegativeButton("Cancel",
                        (sender, args) =>
                        {
                            toDelete = new List<Reminder>();
                            Toast.MakeText(this, "Delete Canceled", ToastLength.Short).Show();
                        });
                    deleteReminderAlert.Show();
                }
            }
            else
                optionsMenu.FindItem(Resource.Id.action_delete).SetIcon(Resource.Mipmap.ic_action_delete_confirm);
            
            deleteMode = !deleteMode;
        }
    }
}