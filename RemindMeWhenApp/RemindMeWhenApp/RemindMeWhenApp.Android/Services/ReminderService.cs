﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Gms.Maps.Model;
using Android.Locations;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using RemindMeWhenApp.DataObjects;
using RemindMeWhenApp.ServiceLayer;

namespace RemindMeWhenApp.Droid.Services
{
    [Service]
    public class ReminderService : IntentService
    {
        private const int TIMEFRAME_HOURS = 1;
        public ReminderService() : base("ReminderService")
        {

        }

        protected override void OnHandleIntent(Intent intent)
        {
            List<Reminder> reminders = MainController.GetRemindersInTimeFrame(DateTime.Now.AddHours(TIMEFRAME_HOURS));
            foreach (Reminder reminder in reminders)
            {
                SingleReminderReceiver.SetSingleReminder(this, reminder.Time);
            }
        }
        
    }
}