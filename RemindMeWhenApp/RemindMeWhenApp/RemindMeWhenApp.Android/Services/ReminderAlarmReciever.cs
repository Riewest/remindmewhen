﻿using System;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Util;

namespace RemindMeWhenApp.Droid.Services
{
    [BroadcastReceiver]
    public class ReminderAlarmReciever : BroadcastReceiver
    {
        private const string TAG = "ReminderService";
        private const int REMINDER_SEVICE_DELAY = 60;
        public override void OnReceive(Context context, Intent intent)
        {
            PowerManager powerManager = (PowerManager) context.GetSystemService(Context.PowerService);
            PowerManager.WakeLock wakeLock = powerManager.NewWakeLock(WakeLockFlags.Partial, "ReminderService");
            wakeLock.Acquire();
            Intent reminderService = new Intent(context, typeof(ReminderService));
            try
            {
                context.StartService(reminderService);
            }
            catch (Exception ex)
            {
                RieLog.Error(TAG, ex.ToString());
            }

            wakeLock.Release();
        }

        public static void StartReminderService(Context context)
        {
        //    ComponentName bootReceiver = new ComponentName(context, Java.Lang.Class.FromType(typeof(ReminderBootReciever)));
        //    PackageManager pm = context.PackageManager;
        //    pm.SetComponentEnabledSetting(bootReceiver, ComponentEnabledState.Enabled, ComponentEnableOption.DontKillApp);

            RieLog.Info(TAG, "Starting Reminder Service");
            Intent reminderAlarmReciever = new Intent(context, typeof(ReminderAlarmReciever));
            PendingIntent pi = PendingIntent.GetBroadcast(context, 0, reminderAlarmReciever, PendingIntentFlags.UpdateCurrent);
            AlarmManager alarmManager = context.GetSystemService(Context.AlarmService).JavaCast<AlarmManager>();
            alarmManager.SetRepeating(AlarmType.ElapsedRealtimeWakeup, SystemClock.ElapsedRealtime() + 1000, 1000 * 60 * REMINDER_SEVICE_DELAY, pi);
        }
    }
}