﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Util.Functions;
using Javax.Security.Auth;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using RemindMeWhenApp.DataObjects;
using RemindMeWhenApp.ServiceLayer;

namespace RemindMeWhenApp.Droid.Services
{
    [Service]
    public class ScheduledReminderService : IntentService
    {
        private const float MPS_CONST = 44.704F; //100 MPH in mps
        private const int MIN_WAIT_SECONDS = 60;
        private static int count = 0;
        public ScheduledReminderService() : base("TimeBasedReminderService")
        {
            
        }

        protected override void OnHandleIntent(Intent intent)
        {
            List<Reminder> reminders = MainController.GetReadyReminders();
            List<Reminder> timeReady = reminders.FindAll(r => r.ReminderLocation == null);
            foreach (Reminder reminder in timeReady)
            {
                displayNotification("Time Reminder:", reminder.Note);
            }
            MainController.DeleteReminders(timeReady);
            foreach (Reminder reminder in reminders.Except(timeReady))
            {
                processLocationReminder(reminder);
            }
        }

        private void processLocationReminder(Reminder reminder)
        {
            var locator = CrossGeolocator.Current;
            var position = locator.GetPositionAsync(TimeSpan.FromSeconds(10)).Result;
            if (!closeEnough(position, reminder.ReminderLocation, out float distance))
            {
                rescheduleReminder(distance);
                return;
            }
            MainController.DeleteReminders(new List<Reminder>(){reminder});
            displayNotification("Location Reminder:", reminder.Note);
        }

        private void rescheduleReminder(float distance)
        {
            var seconds = distance / MPS_CONST;
            seconds = seconds > MIN_WAIT_SECONDS ? seconds : MIN_WAIT_SECONDS;
            SingleReminderReceiver.SetSingleReminder(this, DateTime.Now.AddSeconds(seconds));
        }


        private bool closeEnough(Position myPosition, ReminderLocation reminderLocation, out float distance)
        {
            if (reminderLocation == null)
            {
                distance = -1;
                return false;
            }
                
            float[] result = new float[1];
            Android.Locations.Location.DistanceBetween(myPosition.Latitude, myPosition.Longitude, reminderLocation.Latitude, reminderLocation.Longitude, result);
            distance = result[0];
            return distance < reminderLocation.RadiusInMeters;
        }

        private void displayNotification(string title, string message)
        {
            Intent reminderIntent = new Intent(this, typeof(MainActivity));
            PendingIntent pendingIntent = PendingIntent.GetActivity(this, 0, reminderIntent, PendingIntentFlags.OneShot);
            Notification.Builder builder = new Notification.Builder(this);
            builder.SetAutoCancel(true)
                .SetContentIntent(pendingIntent)
                .SetContentTitle(title)
                .SetContentText(message)
                .SetSmallIcon(Resource.Mipmap.Icon);

            NotificationManager notificationManager = (NotificationManager)GetSystemService(NotificationService);
            notificationManager.Notify(count++, builder.Build());
        }
    }
}