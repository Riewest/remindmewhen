﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using RemindMeWhenApp.Droid.Services;

namespace RemindMeWhenApp.Droid
{
    [BroadcastReceiver]
    public class ReminderBootReciever : BroadcastReceiver
    {
        private const string TAG = "ReminderBootReciever";
        public override void OnReceive(Context context, Intent intent)
        {
            try
            {
                RieLog.Info(TAG, "ReminderBootReciever activated");
            
                if (intent.Action.Equals("android.intent.action.BOOT_COMPLETED"))
                {
                    ReminderAlarmReciever.StartReminderService(context);
                }
            }
            catch (Exception ex)
            {
                RieLog.Error(TAG, ex.ToString());
            }
            
        }
    }
}