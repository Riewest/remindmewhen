﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Util;
using RemindMeWhenApp.DataObjects;

namespace RemindMeWhenApp.Droid.Services
{
    [BroadcastReceiver]
    public class SingleReminderReceiver : BroadcastReceiver
    {
        private const string TAG = "SingleReminderReciever";
        public override void OnReceive(Context context, Intent intent)
        {
            PowerManager powerManager = (PowerManager)context.GetSystemService(Context.PowerService);
            PowerManager.WakeLock wakeLock = powerManager.NewWakeLock(WakeLockFlags.Partial, "ReminderService");
            wakeLock.Acquire();
            Intent timeBaseReminderService = new Intent(context, typeof(ScheduledReminderService));
            try
            {
                context.StartService(timeBaseReminderService);
            }
            catch (Exception ex)
            {
                RieLog.Error(TAG, ex.ToString());
            }

            wakeLock.Release();
        }

        public static void SetSingleReminder(Context context, DateTime time)
        {
            RieLog.Info(TAG, "Starting Reminder Service");
            Intent singleReminderReceiver = new Intent(context, typeof(SingleReminderReceiver));
            PendingIntent pi = PendingIntent.GetBroadcast(context, 0, singleReminderReceiver, PendingIntentFlags.UpdateCurrent);
            TimeSpan timeDifference = time - DateTime.Now;
            AlarmManager alarmManager = context.GetSystemService(Context.AlarmService).JavaCast<AlarmManager>();
            alarmManager.Set(AlarmType.ElapsedRealtimeWakeup, SystemClock.ElapsedRealtime() + (long)timeDifference.TotalMilliseconds, pi);
        }
    }
}