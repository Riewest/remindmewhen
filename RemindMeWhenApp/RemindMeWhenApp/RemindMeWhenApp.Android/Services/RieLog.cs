﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Java.IO;
using RemindMeWhenApp.ServiceLayer;

namespace RemindMeWhenApp.Droid.Services
{
    public static class RieLog
    {
        private const string LOG_FILE = "RemindMeWhenLog.txt";
        private static bool LOG_TO_FILE = false;
        public static void Info(string tag, string message)
        {
            Log.Info(tag, message);
            saveLog(tag, "INFO", message);
        }

        public static void Warn(string tag, string message)
        {
            Log.Info(tag, message);
            saveLog(tag, "WARN", message);
        }

        public static void Error(string tag, string message)
        {
            Log.Info(tag, message);
            saveLog(tag, "ERROR", message);
        }

        private static void saveLog(string tag, string level, string message)
        {
#if DEBUG
            LOG_TO_FILE = false;
#endif
            if (!LOG_TO_FILE)
                return;
            File file = new File(Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDocuments), LOG_FILE);
            string logText = $"Time-{DateTime.Now:F} : Tag-{tag} : Level-{level} : Message-{message}";
            FileController.SaveText(file.AbsolutePath, logText, true);
        }
    }
}