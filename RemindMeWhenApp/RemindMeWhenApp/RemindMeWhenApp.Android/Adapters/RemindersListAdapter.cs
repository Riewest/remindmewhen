﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RemindMeWhenApp.DataObjects;

namespace RemindMeWhenApp.Droid.Adapters
{
    public class RemindersListAdapter : BaseAdapter<Reminder>
    {
        private Reminder[] items;
        private readonly Activity context;

        public RemindersListAdapter(Activity context, Reminder[] items) : base()
        {
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override Reminder this[int position] => items[position];

        public override int Count => items.Length;

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            Reminder reminder = items[position];
            int noteLength = reminder.Note.Length > 20 ? 20 : reminder.Note.Length;
            View view = convertView ?? context.LayoutInflater.Inflate(Android.Resource.Layout.SimpleListItem2, null); // re-use an existing view, if one is available
            view.FindViewById<TextView>(Android.Resource.Id.Text1).Text = reminder.Note.Substring(0, noteLength) + "...";
            view.FindViewById<TextView>(Android.Resource.Id.Text2).Text = reminder.Time.ToString("h:mm tt MMM d");
            return view;
        }

        public void ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
             Reminder[] dest = new Reminder[items.Length - 1];
            if(e.Position > 0)
                Array.Copy(items, 0, dest, 0, e.Position);
            if(e.Position < items.Length - 1)
                Array.Copy(items, e.Position + 1, dest, e.Position, items.Length - e.Position - 1);
            ResetItems(dest);
        }

        public void ResetItems(Reminder[] newItems)
        {
            items = newItems;
            NotifyDataSetChanged();
        }
    }
}