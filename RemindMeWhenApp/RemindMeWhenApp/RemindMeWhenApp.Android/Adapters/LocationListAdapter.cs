﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RemindMeWhenApp.DataObjects;

namespace RemindMeWhenApp.Droid.Adapters
{
    public class LocationListAdapter : BaseAdapter<ReminderLocation>
    {
        private ReminderLocation[] items;
        private Activity context;

        public LocationListAdapter(Activity context, ReminderLocation[] items) : base()
        {
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override ReminderLocation this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Length; }
        }
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = convertView; // re-use an existing view, if one is available
            if (view == null) // otherwise create a new one
                view = context.LayoutInflater.Inflate(Android.Resource.Layout.SimpleListItem1, null);
            view.FindViewById<TextView>(Android.Resource.Id.Text1).Text = items[position].Name;
            return view;
        }

        public void ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {

        }

        public void ResetItems(ReminderLocation[] newItems)
        {
            items = newItems;
            NotifyDataSetChanged();
            
        }
    }
}