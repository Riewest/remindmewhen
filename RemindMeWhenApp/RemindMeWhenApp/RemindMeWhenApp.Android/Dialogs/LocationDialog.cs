﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RemindMeWhenApp.DataObjects;
using RemindMeWhenApp.Droid.Adapters;
using RemindMeWhenApp.ServiceLayer;
using Void = Java.Lang.Void;

namespace RemindMeWhenApp.Droid.Dialogs
{
    public class DialogEventArgs : EventArgs
    {
        public ReminderLocation ReminderLocation { get; set; }
    }

    public delegate void DialogEventHandler(object sender, DialogEventArgs args);

    public class LocationDialog : Dialog
    {
        private List<ReminderLocation> locations;
        private Button newLocationButton;
        private ListView locationsListView;
        private LocationListAdapter adapter;
        private Activity context;
        public event DialogEventHandler Dismissed;
        public LocationDialog(Activity context, List<ReminderLocation> locations) : base(context)
        {
            this.context = context;
            this.locations = locations;
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature((int)WindowFeatures.NoTitle);
            SetContentView(Resource.Layout.LocationDialog);
            locationsListView = FindViewById<ListView>(Resource.Id.locationsListView);
            adapter = new LocationListAdapter(context, locations.ToArray());
            locationsListView.Adapter = adapter;
            locationsListView.ItemClick += OnLocationClick;
            locationsListView.ItemLongClick += OnLocationLongClick;
            newLocationButton = FindViewById<Button>(Resource.Id.newLocationBtn);
            newLocationButton.Click += newLocationButtonClick;
        }

        private void OnLocationClick(object sender, AdapterView.ItemClickEventArgs ea)
        {
            endDialog(adapter[ea.Position]);
        }

        private void OnLocationLongClick(object sender, AdapterView.ItemLongClickEventArgs ea)
        {
            ReminderLocation toDelete = adapter[ea.Position];
            AlertDialog.Builder deleteDialog = new AlertDialog.Builder(context);
            deleteDialog.SetTitle("Delete Location?");
            deleteDialog.SetMessage($"Location: {toDelete.Name}");
            deleteDialog.SetPositiveButton("Yes",
                (o, args) =>
                {
                    MainController.DeleteLocation(new List<ReminderLocation>() {toDelete});
                    locations = MainController.Locations;
                    adapter.ResetItems(locations.ToArray());
                });
            deleteDialog.SetNegativeButton("No", ((o, args) => {}));
            deleteDialog.Show();
        }

        private void newLocationButtonClick(object sender, EventArgs eventArgs)
        {
            endDialog(null);
        }

        private void endDialog(ReminderLocation reminderLocation)
        {
            Dismissed?.Invoke(this, new DialogEventArgs { ReminderLocation = reminderLocation });
            this.Dismiss();
        }
    }

    
}