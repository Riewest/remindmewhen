﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Gms.Location.Places.UI;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.Locations;
using Android.OS;
using Android.Support.V4.App;
using Android.Support.V4.Media;
using Android.Text;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using Newtonsoft.Json;
using Plugin.Geolocator;
using RemindMeWhenApp.DataObjects;
using RemindMeWhenApp.Droid.Services;
using RemindMeWhenApp.ServiceLayer;
using Result = Android.App.Result;

namespace RemindMeWhenApp.Droid
{

    [Activity(Label = "NewLocationActivity")]
    public class NewLocationActivity : Activity, IOnMapReadyCallback
    {
        private const string TAG = "RMWNewLocation";
        private const int RADIUS_MAX = 1000;
        private const int RADIUS_MIN = 50;
        private const int RADIUS_DEFAULT = 3;
        private const int STEP_SIZE = 50;
        private const int PLACE_PICKER_REQUEST = 1;

        private EditText locationNamEditText;
        private Button createButton;
        private Button pickAPlaceButton;
        private SeekBar radiusSeekBar;
        private TextView radiusTextView;
        private GoogleMap map;
        private Marker selectedLocation;
        private Circle selectedRadius;
        
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.NewLocation);

            ApplicationInfo ai = PackageManager.GetApplicationInfo(PackageName, PackageInfoFlags.MetaData);
            

            initControls();
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            if (requestCode == PLACE_PICKER_REQUEST && resultCode == Result.Ok)
            {
                GetPlaceFromPicker(data);
            }

            base.OnActivityResult(requestCode, resultCode, data);
        }

        private void initControls()
        {
            MapFragment mapFragment = FragmentManager.FindFragmentById<MapFragment>(Resource.Id.map);
            locationNamEditText = FindViewById<EditText>(Resource.Id.locationNameETxt);
            radiusSeekBar = FindViewById<SeekBar>(Resource.Id.radiusSeekBar);
            radiusTextView = FindViewById<TextView>(Resource.Id.radiusText);
            pickAPlaceButton = FindViewById<Button>(Resource.Id.findPlaceBtn);
            createButton = FindViewById<Button>(Resource.Id.createBtn);
            createButton.Click += createButtonClick;
            pickAPlaceButton.Click += pickAPlaceClick;
            radiusSeekBar.Max = (RADIUS_MAX-RADIUS_MIN) / STEP_SIZE;
            radiusSeekBar.Progress = RADIUS_DEFAULT;
            radiusSeekBar.ProgressChanged += radiusChanged;
            radiusTextView.Text = $"Radius {getRadius()}(m)";
            //locationNamEditText.TextChanged += locationNameTextChanged;
            mapFragment.GetMapAsync(this);
        }

        public void OnMapReady(GoogleMap map)
        {
            this.map = map;
            map.MapClick += mapClicked;
            setStartLocation();
        }

        private void GetPlaceFromPicker(Intent data)
        {
            var placePicked = PlacePicker.GetPlace(this, data);

            LatLng position = placePicked?.LatLng;
            updateCameraPosition(position);
            placeMarker(position);
        }

        private void locationNameTextChanged(object sender, TextChangedEventArgs eventArgs)
        {
            if (eventArgs.AfterCount > eventArgs.BeforeCount)
                locationNamEditText.Text = locationNamEditText.Text.Replace("\n", "");
        }
        
        private void radiusChanged(object sender, SeekBar.ProgressChangedEventArgs eventArgs)
        {
            radiusTextView.Text = $"Radius {(eventArgs.Progress + RADIUS_MIN/STEP_SIZE) * STEP_SIZE}(m)";
            addCircle(selectedLocation.Position);
        }

        private void mapClicked(object sender, GoogleMap.MapClickEventArgs eventArgs)
        {
            placeMarker(eventArgs.Point);
        }

        private void pickAPlaceClick(object sender, EventArgs eventArgs)
        {
            var builder = new PlacePicker.IntentBuilder();
            StartActivityForResult(builder.Build(this), PLACE_PICKER_REQUEST);
        }

        private void createButtonClick(object sender, EventArgs eventArgs)
        {
            string locationName = locationNamEditText.Text;
            if (locationName == "")
            {
                Toast.MakeText(this, "Please Enter a Name.", ToastLength.Long).Show();
                return;
            }
            Intent resultIntent = new Intent(this, typeof(MainActivity));
            resultIntent.PutExtra("LocationName", locationName);
            ReminderLocation newLocation = new ReminderLocation()
            {
                Name = locationName,
                Latitude = selectedLocation.Position.Latitude,
                Longitude = selectedLocation.Position.Longitude,
                RadiusInMeters = getRadius()
            };
            RieLog.Info(TAG, $"Creating New Location: {newLocation.Name}, {newLocation.Latitude}, {newLocation.Longitude}");
            MainController.CreateLocation(newLocation);
            SetResult(Result.Ok, resultIntent);
            Finish();
        }

        private async void setStartLocation()
        {
            var locator = CrossGeolocator.Current;
            var position = await locator.GetLastKnownLocationAsync();
            var latLng = new LatLng(position.Latitude, position.Longitude);
            updateCameraPosition(latLng);
            placeMarker(latLng);
        }

        void updateCameraPosition(LatLng pos)
        {
            CameraPosition.Builder builder = CameraPosition.InvokeBuilder();
            builder.Target(pos);
            builder.Zoom(13);
            CameraPosition cameraPosition = builder.Build();
            CameraUpdate cameraUpdate = CameraUpdateFactory.NewCameraPosition(cameraPosition);
            map.AnimateCamera(cameraUpdate);
        }

        private void placeMarker(LatLng point)
        {
            selectedLocation?.Remove();
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.SetPosition(point);
            markerOptions.SetTitle("New Location");
            selectedLocation = map.AddMarker(markerOptions);
            addCircle(point);
        }

        private void addCircle(LatLng point)
        {
            selectedRadius?.Remove();
            CircleOptions circleOptions = new CircleOptions();
            circleOptions.InvokeCenter(point);
            circleOptions.InvokeRadius(getRadius());
            circleOptions.InvokeFillColor(unchecked((int) 0x800099ff));
            circleOptions.InvokeStrokeColor(0x003d66);
            circleOptions.InvokeStrokeWidth(7);
            selectedRadius = map.AddCircle(circleOptions);
        }

        private int getRadius()
        {
            return (radiusSeekBar.Progress + RADIUS_MIN / STEP_SIZE) * STEP_SIZE;
        }
    }
}