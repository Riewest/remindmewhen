﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RemindMeWhenApp.DataObjects
{
    public class Reminder
    {
        public string Note { get; set; }
        public DateTime Time { get; set; }
        public ReminderLocation ReminderLocation { get; set; }
    }
}
