﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using RemindMeWhenApp.DataObjects;

namespace RemindMeWhenApp.ServiceLayer
{
    public static class MainController
    {
        private static readonly string DATA_FOLDER = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
        private const string REMINDERS_FILE = "Reminders";
        private const string LOCATIONS_FILE = "Locations";
        private const int UPCOMING_REMINDER_LIST_SIZE = 3;
        public static List<Reminder> Reminders { get; set; }
        public static List<ReminderLocation> Locations { get; set; }

        public static void Init()
        {
            initializeData();
        }

        public static List<Reminder> GetUpcomingReminders()
        {
            List<Reminder> nextReminders = new List<Reminder>();
            //Order By Time
            nextReminders.AddRange(Reminders.OrderBy(r => r.Time).Take(UPCOMING_REMINDER_LIST_SIZE));
            return nextReminders;
        }

        public static List<Reminder> GetRemindersInTimeFrame(DateTime time)
        {
            if (Reminders == null)
                initializeData();
            return Reminders.FindAll(r => r.Time < time);
        }

        public static List<Reminder> GetReadyReminders()
        {
            if(Reminders ==  null)
                initializeData();
            return Reminders.FindAll(r => DateTime.Now > r.Time);
        }
        

        public static void CreateReminder(Reminder newReminder)
        {
            Reminders.Add(newReminder);
            saveReminders();
        }

        public static void CreateLocation(ReminderLocation reminderLocation)
        {
            Locations.Add(reminderLocation);
            saveLocations();
        }

        public static void DeleteLocation(List<ReminderLocation> toDelete)
        {
            Locations = Locations.Except(toDelete).ToList();
            saveLocations();
        }

        public static void DeleteReminders(List<Reminder> toDelete)
        {
            Reminders = Reminders.Except(toDelete).ToList();
            saveReminders();
        }

        private static void initializeData()
        {
            Locations = retrieveLocations();
            Reminders = retrieveReminders();
        }

        private static List<Reminder> retrieveReminders()
        {
            List<Reminder> reminders = FileController.ReadList<Reminder>(DATA_FOLDER, REMINDERS_FILE);
#if DEBUG
            //reminders = new List<Reminder>()
            //{
            //    new Reminder(){Note = "TestNote1", ReminderLocation = Locations.First(), Time = DateTime.Now.AddMinutes(1)},
            //    new Reminder(){Note = "TestNote2", ReminderLocation = Locations.First(), Time = DateTime.Now.AddMinutes(2)},
            //    new Reminder(){Note = "TestNote3", ReminderLocation = Locations.First(), Time = DateTime.Now.AddMinutes(3)},
            //    new Reminder(){Note = "TestNote4", ReminderLocation = Locations.First(), Time = DateTime.Now.AddMinutes(4)}
            //};
#endif
            return reminders;
        }

        private static List<ReminderLocation> retrieveLocations()
        {
            List<ReminderLocation> locations = FileController.ReadList<ReminderLocation>(DATA_FOLDER, LOCATIONS_FILE);
#if DEBUG
            //locations = new List<Location>()
            //{
            //    new Location(){Name = "Test Location 1"},
            //    new Location(){Name = "Test Location 2"},
            //    new Location(){Name = "Test Location 3"}
            //};
#endif
            return locations;
        }

        private static void saveReminders()
        {
            FileController.SaveList(DATA_FOLDER, REMINDERS_FILE, Reminders);
        }

        private static  void saveLocations()
        {
            FileController.SaveList(DATA_FOLDER, LOCATIONS_FILE, Locations);
        }


    }
}
