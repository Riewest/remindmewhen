﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace RemindMeWhenApp.ServiceLayer
{
    public static class FileController
    {
        public static void SaveList<T>(string path, string file, List<T> list, bool append = false)
        {
            SaveText(path, file, JsonConvert.SerializeObject(list), append);
        }

        public static List<T> ReadList<T>(string path, string file)
        {
            string combinedPath = Path.Combine(path, file);
            try
            {
                using (var streamReader = new StreamReader(combinedPath))
                {
                    string content = streamReader.ReadToEnd();
                    return JsonConvert.DeserializeObject<List<T>>(content);
                }
            }
            catch (FileNotFoundException)
            {
                return new List<T>();
            }
            
        }

        public static void SaveText(string path, string file, string text, bool append = false)
        {
            string combinedPath = Path.Combine(path, file);
            SaveText(combinedPath, text, append);
        }

        public static void SaveText(string fullPath, string text, bool append = false)
        {
            Console.WriteLine($"Path: {fullPath}");
            using (var streamWriter = new StreamWriter(fullPath, append))
            {
                streamWriter.WriteLine(text);
            }
        }
    }
}
