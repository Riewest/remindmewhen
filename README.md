RemindMeWhen

Reminder
	Time and/or Location Based
	Note --140 char limit?
	
Activities
	Login
		Auth0
	Main
		Upcoming Reminders
			Next 3
				Time First
				Then pulls from most recent added
		Add Reminder
			Location
				Top 5
				Option to pick a different location
				Dialog with a list
				Option to pick new Location
				Distance away
			Time
				Dialog Picker Date/Time
			Note
				Possible Char Limit
		View My Reminders
			ViewReminders Activity
	ViewReminders
		List of reminders
			Order by remind time then date/time created
		Select Reminder
	PickLocation
		GoogleMaps
		Save Location
			Name
			Category
	Settings
		Theme
		
		